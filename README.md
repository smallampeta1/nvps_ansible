# NVPS_Ansible

```
source ansible-venv/bin/activate

git clone git@gitlab.com:smallampeta1/nvps_ansible.git

cd nvps_ansible
```

## Ansible Playbook deploy
```
ansible-playbook -i inventories/evpn_symmetric/hosts playbooks/deploy.yml -l leaf01
```